import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { IEntity } from "./acct-manage/account-setup/IEntity";
import { IAccountDB} from "./acct-manage/account-setup/IAccountDB";
import { IAccountEntity } from "./acct-manage/account-setup/IAccountEntity";
import { environment } from "../environments/environment";
import {IInvoice} from "./acct-manage/account-setup/IInvoice";
import {IEntityType} from "./acct-manage/account-setup/IEntityType";
import {ILicenseGroup} from "./acct-manage/account-setup/ILicenseGroup";
import {ILicenseEntityAllocation} from "./acct-manage/account-setup/ILicenseEntityAllocation";
import {ILicenseConsumerGroup} from "./acct-manage/account-setup/ILicenseConsumerGroup";
import { pipe, forkJoin } from "rxjs";
import {map} from "rxjs/operators";
import {IRole} from "./acct-manage/account-setup/IRole";

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) {

  }

  // ACCOUNT
  public getAccounts(): Observable<IAccountDB[]> {
    return this.http.get<IAccountDB[]>(this.apiUrl + 'Accounts');
  }
  public getAccount(id): Observable<IAccountDB[]>{
    return this.http.get<IAccountDB[]>(this.apiUrl + 'Accounts/' + id);
  }
  public createAccount(account){
    return this.http.post(this.apiUrl + 'Accounts/', account, {headers:{'Content-Type': 'application/json'}});
  }
  public editAccount(account): Observable<IAccountDB[]> {
    return this.http.put<IAccountDB[]>(this.apiUrl + 'Accounts/' + account['accountId'], account, {headers:{'Content-Type': 'application/json'}} );
  }
  public deleteAccount(id): Observable<IAccountDB[]> {
    return this.http.delete<IAccountDB[]>(this.apiUrl + 'Accounts/' + id);
  }

  // ACCOUNT ENTITIES
  public getAccountEntities(): Observable<IAccountEntity[]> {
    return this.http.get<IAccountEntity[]>(this.apiUrl + 'AccountEntities');
  }
  public getAccountEntity(id): Observable<IAccountEntity[]>{
    return this.http.get<IAccountEntity[]>(this.apiUrl + 'AccountEntities/' + id);
  }
  public createAccountEntity(accountEntity){
    return this.http.post(this.apiUrl + 'AccountEntities/', accountEntity, {headers:{'Content-Type': 'application/json'}});
  }
  public editAccountEntity(accountEntity): Observable<IAccountEntity[]> {
    return this.http.put<IAccountEntity[]>(this.apiUrl + 'AccountEntities/' + accountEntity['accountEntityId'], accountEntity, {headers:{'Content-Type': 'application/json'}} );
  }
  public deleteAccountEntity(id): Observable<IAccountEntity[]> {
    return this.http.delete<IAccountEntity[]>(this.apiUrl + 'AccountEntities/' + id);
  }

  // ENTITIES
  public getEntities(): Observable<IEntity[]> {
    return this.http.get<IEntity[]>(this.apiUrl + 'Entities/');
  }
  public getEntity(id): Observable<IEntity[]> {
    return this.http.get<IEntity[]>(this.apiUrl + 'Entities/' + id);
  }
  public createEntity(entity){
    return this.http.post(this.apiUrl + 'Entities/', entity, {headers:{'Content-Type': 'application/json'}});
  }
  public editEntity(entity): Observable<IEntity[]> {
    return this.http.put<IEntity[]>(this.apiUrl + 'Entities/' + entity['entityId'], entity, {headers:{'Content-Type': 'application/json'}} );
  }
  public deleteEntity(id): Observable<IEntity[]> {
    return this.http.delete<IEntity[]>(this.apiUrl + 'Entities/' + id);
  }

  // ENTITY TYPES
  public getEntityTypes(): Observable<IEntityType[]> {
    return this.http.get<IEntityType[]>(this.apiUrl + 'EntityTypes/');
  }
  public getEntityType(id): Observable<IEntityType[]> {
    return this.http.get<IEntityType[]>(this.apiUrl + 'EntityTypes/' + id);
  }
  public createEntityType(entityType){
    return this.http.post(this.apiUrl + 'EntityTypes/', entityType, {headers:{'Content-Type': 'application/json'}});
  }
  public editEntityType(entity): Observable<IEntityType[]> {
    return this.http.put<IEntityType[]>(this.apiUrl + 'EntityTypes/' + entity['entityTypeId'], entity, {headers:{'Content-Type': 'application/json'}} );
  }
  public deleteEntityType(id): Observable<IEntityType[]> {
    return this.http.delete<IEntityType[]>(this.apiUrl + 'EntityTypes/' + id);
  }

  //INVOICES
  public getInvoices(): Observable<IInvoice[]> {
    return this.http.get<IInvoice[]>(this.apiUrl + 'Invoices');
  }
  public getInvoice(id): Observable<IInvoice[]> {
    return this.http.get<IInvoice[]>(this.apiUrl + 'Invoices/' + id);
  }
  public createInvoice(invoice){
    return this.http.post(this.apiUrl + 'Invoices/', invoice, {headers:{'Content-Type': 'application/json'}});
  }
  public editInvoice(invoice): Observable<IInvoice[]> {
    return this.http.put<IInvoice[]>(this.apiUrl + 'Invoices/' + invoice['invoiceId'], invoice, {headers:{'Content-Type': 'application/json'}} );
  }
  public deleteInvoice(id): Observable<IInvoice[]> {
    return this.http.delete<IInvoice[]>(this.apiUrl + 'Invoices/' + id);
  }

  // LICENSE GROUPS
  public getLicenseGroups(): Observable<ILicenseGroup[]> {
    return this.http.get<ILicenseGroup[]>(this.apiUrl + 'LicenseGroups');
  }
  public getLicenseGroup(id): Observable<ILicenseGroup[]> {
    return this.http.get<ILicenseGroup[]>(this.apiUrl + 'LicenseGroups/' + id);
  }
  public createLicenseGroup(licenseGroup){
    return this.http.post(this.apiUrl + 'LicenseGroups/', licenseGroup, {headers:{'Content-Type': 'application/json'}});
  }
  public editLicenseGroup(licenseGroup): Observable<ILicenseGroup[]> {
    console.log(licenseGroup);
    return this.http.put<ILicenseGroup[]>(this.apiUrl + 'LicenseGroups/' + licenseGroup['licenseGroupId'], licenseGroup, {headers:{'Content-Type': 'application/json'}} );
  }
  public deleteLicenseGroup(id): Observable<ILicenseGroup[]> {
    return this.http.delete<ILicenseGroup[]>(this.apiUrl + 'LicenseGroups/' + id);
  }

  // LICENSE ENTITY ALLOCATIONS
  public getAllocations(): Observable<ILicenseEntityAllocation[]> {
    return this.http.get<ILicenseEntityAllocation[]>(this.apiUrl + 'LicenseEntityAllocations');
  }
  public getLicenseEntityAllocation(id): Observable<ILicenseEntityAllocation[]> {
    return this.http.get<ILicenseEntityAllocation[]>(this.apiUrl + 'LicenseEntityAllocations/' + id);
  }
  public editLicenseEntityAllocation(licenseEntityAllocation): Observable<ILicenseEntityAllocation[]> {
    return this.http.put<ILicenseEntityAllocation[]>(this.apiUrl + 'LicenseEntityAllocations/' + licenseEntityAllocation['licenseEntityAllocationId'], licenseEntityAllocation, {headers:{'Content-Type': 'application/json'}} );
  }
  public createLicenseEntityAllocation(licenseEntityAllocation){
    return this.http.post(this.apiUrl + 'LicenseEntityAllocations/', licenseEntityAllocation, {headers:{'Content-Type': 'application/json'}});
  }
  public deleteLicenseEntityAllocation(id): Observable<ILicenseEntityAllocation[]> {
    return this.http.delete<ILicenseEntityAllocation[]>(this.apiUrl + 'LicenseEntityAllocations/' + id);
  }

  // LICENSE CONSUMER GROUPS
  public getLicenseConsumerGroups(): Observable<ILicenseConsumerGroup[]> {
    return this.http.get<ILicenseConsumerGroup[]>(this.apiUrl + 'LicenseConsumerGroups');
  }
  public createLicenseConsumerGroup(group){
    return this.http.post(this.apiUrl + 'LicenseConsumerGroups/', group, {headers:{'Content-Type': 'application/json'}});
  }

  // ROLES
  public getRoles(): Observable<IRole[]> {
    return this.http.get<IRole[]>(this.apiUrl + 'Roles');
  }
  public getRole(id): Observable<IRole[]> {
    return this.http.get<IRole[]>(this.apiUrl + 'Roles/' + id);
  }
  public editRole(role): Observable<IRole[]> {
    console.log(role);
    return this.http.put<IRole[]>(this.apiUrl + 'Roles/' + role['roleId'], role, {headers:{'Content-Type': 'application/json'}} );
  }
  public createRole(role){
    return this.http.post(this.apiUrl + 'Roles/', role, {headers:{'Content-Type': 'application/json'}});
  }
  public deleteRole(id): Observable<IRole[]> {
    return this.http.delete<IRole[]>(this.apiUrl + 'Roles/' + id);
  }

}
