import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AccountSetupComponent} from "./acct-manage/account-setup/account-setup.component";
import {CreateAccountComponent} from "./acct-manage/account-setup/accounts/create/create-account.component";
import {CreateEntityTypeComponent} from "./acct-manage/account-setup/entity-types/create/create-entity-type.component";
import {CreateAccountEntityComponent} from "./acct-manage/account-setup/account-entities/create/create-account-entity.component";
import {CreateEntityComponent} from "./acct-manage/account-setup/entities/create/create-entity.component";
import {ViewEntityTypesComponent} from "./acct-manage/account-setup/entity-types/view/view-entity-types.component";
import {EditEntityTypeComponent} from "./acct-manage/account-setup/entity-types/edit/edit-entity-type.component";
import {ViewEntitiesComponent} from "./acct-manage/account-setup/entities/view/view-entities.component";
import {EditEntityComponent} from "./acct-manage/account-setup/entities/edit/edit-entity.component";
import {DeleteEntityComponent} from "./acct-manage/account-setup/entities/delete/delete-entity.component";
import {ViewAccountEntitiesComponent} from "./acct-manage/account-setup/account-entities/view/view-account-entities.component";
import {EditAccountEntityComponent} from "./acct-manage/account-setup/account-entities/edit/edit-account-entity.component";
import {DeleteAccountEntityComponent} from "./acct-manage/account-setup/account-entities/delete/delete-account-entity.component";
import {ViewAccountsComponent} from "./acct-manage/account-setup/accounts/view/view-accounts.component";
import {DeleteAccountComponent} from "./acct-manage/account-setup/accounts/delete/delete-account.component";
import {EditAccountComponent} from "./acct-manage/account-setup/accounts/edit/edit-account.component";
import {CreateInvoiceComponent} from "./acct-manage/account-setup/invoices/create/create-invoice.component";
import {ViewInvoicesComponent} from "./acct-manage/account-setup/invoices/view/view-invoices.component";
import {DeleteInvoiceComponent} from "./acct-manage/account-setup/invoices/delete/delete-invoice.component";
import {EditInvoiceComponent} from "./acct-manage/account-setup/invoices/edit/edit-invoice.component";
import {DeleteEntityTypeComponent} from "./acct-manage/account-setup/entity-types/delete/delete-entity-type.component";
import {ViewLicenseGroupsComponent} from "./acct-manage/account-setup/license-groups/view/view-license-groups.component";
import {CreateLicenseGroupComponent} from "./acct-manage/account-setup/license-groups/create/create-license-group.component";
import {DeleteLicenseGroupComponent} from "./acct-manage/account-setup/license-groups/delete/delete-license-group.component";
import {EditLicenseGroupComponent} from "./acct-manage/account-setup/license-groups/edit/edit-license-group.component";
import {CreateLicenseEntityAllocationComponent} from "./acct-manage/account-setup/license-entity-allocations/create/create-license-entity-allocation.component";
import {ViewLicenseEntityAllocationComponent} from "./acct-manage/account-setup/license-entity-allocations/view/view-license-entity-allocation.component";
import {CreateLicenseConsumerGroupComponent} from "./acct-manage/account-setup/license-consumer-group/create/create-license-consumer-group.component";
import {ViewLicenseConsumerGroupsComponent} from "./acct-manage/account-setup/license-consumer-group/view/view-license-consumer-groups.component";
import {EditLicenseEntityAllocationComponent} from "./acct-manage/account-setup/license-entity-allocations/edit/edit-license-entity-allocation.component";
import {DeleteLicenseEntityAllocationComponent} from "./acct-manage/account-setup/license-entity-allocations/delete/delete-license-entity-allocation.component";
import {CreateAccountEntityUserComponent} from "./acct-manage/account-setup/account-entity-users/create/create-account-entity-user.component";
import {CreateRoleComponent} from "./acct-manage/account-setup/roles/create/create-role.component";
import {ViewRolesComponent} from "./acct-manage/account-setup/roles/view/view-roles.component";
import {EditRoleComponent} from "./acct-manage/account-setup/roles/edit/edit-role.component";
import {DeleteRoleComponent} from "./acct-manage/account-setup/roles/delete/delete-role.component";
import {CreateUserRoleComponent} from "./acct-manage/account-setup/user-roles/create/create-user-role.component";

const routes: Routes = [
  { path: '', redirectTo: 'account-setup', pathMatch: 'full' },
  { path: 'account-setup', component: AccountSetupComponent},
  { path: 'account-setup/accounts/create', component: CreateAccountComponent },
  { path: 'account-setup/accounts/view', component: ViewAccountsComponent },
  { path: 'account-setup/accounts/delete/:id', component: DeleteAccountComponent },
  { path: 'account-setup/accounts/edit/:id', component: EditAccountComponent },
  { path: 'account-setup/entities/create', component: CreateEntityComponent },
  { path: 'account-setup/entities/view', component: ViewEntitiesComponent },
  { path: 'account-setup/entities/edit/:id', component: EditEntityComponent },
  { path: 'account-setup/entities/delete/:id', component: DeleteEntityComponent },
  { path: 'account-setup/entity-types/create', component: CreateEntityTypeComponent },
  { path: 'account-setup/entity-types/delete/:id', component: DeleteEntityTypeComponent },
  { path: 'account-setup/account-entities/create', component: CreateAccountEntityComponent },
  { path: 'account-setup/account-entities/view', component: ViewAccountEntitiesComponent },
  { path: 'account-setup/account-entities/edit/:id', component: EditAccountEntityComponent },
  { path: 'account-setup/account-entities/delete/:id', component: DeleteAccountEntityComponent },
  { path: 'account-setup/entity-types/view', component: ViewEntityTypesComponent },
  { path: 'account-setup/entity-types/edit/:id', component: EditEntityTypeComponent },
  { path: 'account-setup/entity-types/delete/:id', component: DeleteEntityTypeComponent },
  { path: 'account-setup/invoices/create', component: CreateInvoiceComponent },
  { path: 'account-setup/invoices/view', component: ViewInvoicesComponent },
  { path: 'account-setup/invoices/delete/:id', component: DeleteInvoiceComponent },
  { path: 'account-setup/invoices/edit/:id', component: EditInvoiceComponent },
  { path: 'account-setup/license-groups/create', component: CreateLicenseGroupComponent },
  { path: 'account-setup/license-groups/view', component: ViewLicenseGroupsComponent },
  { path: 'account-setup/license-groups/delete/:id', component: DeleteLicenseGroupComponent },
  { path: 'account-setup/license-groups/edit/:id', component: EditLicenseGroupComponent },
  { path: 'account-setup/license-entity-allocations/create', component: CreateLicenseEntityAllocationComponent },
  { path: 'account-setup/license-entity-allocations/view', component: ViewLicenseEntityAllocationComponent },
  { path: 'account-setup/license-entity-allocations/edit/:id', component: EditLicenseEntityAllocationComponent },
  { path: 'account-setup/license-entity-allocations/delete/:id', component: DeleteLicenseEntityAllocationComponent },
  { path: 'account-setup/license-consumer-groups/create', component: CreateLicenseConsumerGroupComponent },
  { path: 'account-setup/license-consumer-groups/view', component: ViewLicenseConsumerGroupsComponent },
  { path: 'account-setup/account-entity-users/create', component: CreateAccountEntityUserComponent },
  { path: 'account-setup/roles/create', component: CreateRoleComponent },
  { path: 'account-setup/roles/view', component: ViewRolesComponent },
  { path: 'account-setup/roles/edit/:id', component: EditRoleComponent },
  { path: 'account-setup/roles/delete/:id', component: DeleteRoleComponent },
  { path: 'account-setup/user-roles/create', component: CreateUserRoleComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
