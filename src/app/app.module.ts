import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccountSetupComponent } from './acct-manage/account-setup/account-setup.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatBottomSheetModule,
  MatBadgeModule
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AccountComponent } from './account/account.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './api.service';
import { CreateAccountComponent } from './acct-manage/account-setup/accounts/create/create-account.component';
import { CreateEntityComponent } from './acct-manage/account-setup/entities/create/create-entity.component';
import { CreateEntityTypeComponent } from './acct-manage/account-setup/entity-types/create/create-entity-type.component';
import { CreateAccountEntityComponent } from './acct-manage/account-setup/account-entities/create/create-account-entity.component';
import { ViewEntityTypesComponent } from './acct-manage/account-setup/entity-types/view/view-entity-types.component';
import { EntityComponent } from './acct-manage/account-setup/entities/entity/entity.component';
import { EditEntityTypeComponent } from './acct-manage/account-setup/entity-types/edit/edit-entity-type.component';
import { SidebarComponent } from './acct-manage/account-setup/shared/sidebar/sidebar.component';
import { ViewEntitiesComponent } from './acct-manage/account-setup/entities/view/view-entities.component';
import { EditEntityComponent } from './acct-manage/account-setup/entities/edit/edit-entity.component';
import { DeleteEntityComponent } from './acct-manage/account-setup/entities/delete/delete-entity.component';
import { ViewAccountEntitiesComponent } from './acct-manage/account-setup/account-entities/view/view-account-entities.component';
import { EditAccountEntityComponent } from './acct-manage/account-setup/account-entities/edit/edit-account-entity.component';
import { DeleteAccountEntityComponent } from './acct-manage/account-setup/account-entities/delete/delete-account-entity.component';
import { ViewAccountsComponent } from './acct-manage/account-setup/accounts/view/view-accounts.component';
import { DeleteAccountComponent } from './acct-manage/account-setup/accounts/delete/delete-account.component';
import { EditAccountComponent } from './acct-manage/account-setup/accounts/edit/edit-account.component';
import { CreateInvoiceComponent } from './acct-manage/account-setup/invoices/create/create-invoice.component';
import { ViewInvoicesComponent } from './acct-manage/account-setup/invoices/view/view-invoices.component';
import { DeleteInvoiceComponent } from './acct-manage/account-setup/invoices/delete/delete-invoice.component';
import { EditInvoiceComponent } from './acct-manage/account-setup/invoices/edit/edit-invoice.component';
import { DeleteEntityTypeComponent } from './acct-manage/account-setup/entity-types/delete/delete-entity-type.component';
import { CreateLicenseGroupComponent } from './acct-manage/account-setup/license-groups/create/create-license-group.component';
import { ViewLicenseGroupsComponent } from './acct-manage/account-setup/license-groups/view/view-license-groups.component';
import { DeleteLicenseGroupComponent } from './acct-manage/account-setup/license-groups/delete/delete-license-group.component';
import { EditLicenseGroupComponent } from './acct-manage/account-setup/license-groups/edit/edit-license-group.component';
import { CreateLicenseEntityAllocationComponent } from './acct-manage/account-setup/license-entity-allocations/create/create-license-entity-allocation.component';
import { ViewLicenseEntityAllocationComponent } from './acct-manage/account-setup/license-entity-allocations/view/view-license-entity-allocation.component';
import { CreateLicenseConsumerGroupComponent } from './acct-manage/account-setup/license-consumer-group/create/create-license-consumer-group.component';
import { ViewLicenseConsumerGroupsComponent } from './acct-manage/account-setup/license-consumer-group/view/view-license-consumer-groups.component';
import { EditLicenseEntityAllocationComponent } from './acct-manage/account-setup/license-entity-allocations/edit/edit-license-entity-allocation.component';
import { DeleteLicenseEntityAllocationComponent } from './acct-manage/account-setup/license-entity-allocations/delete/delete-license-entity-allocation.component';
import { CreateAccountEntityUserComponent } from './acct-manage/account-setup/account-entity-users/create/create-account-entity-user.component';
import { CreateRoleComponent } from './acct-manage/account-setup/roles/create/create-role.component';
import { ViewRolesComponent } from './acct-manage/account-setup/roles/view/view-roles.component';
import { EditRoleComponent } from './acct-manage/account-setup/roles/edit/edit-role.component';
import { DeleteRoleComponent } from './acct-manage/account-setup/roles/delete/delete-role.component';
import { CreateUserRoleComponent } from './acct-manage/account-setup/user-roles/create/create-user-role.component';
import { CreateUserComponent } from './acct-manage/account-setup/users/create-user/create-user.component';

@NgModule({
  declarations: [
    AppComponent,
    AccountSetupComponent,
    AccountComponent,
    CreateAccountComponent,
    CreateEntityComponent,
    CreateEntityTypeComponent,
    CreateAccountEntityComponent,
    ViewEntityTypesComponent,
    EntityComponent,
    EditEntityTypeComponent,
    SidebarComponent,
    ViewEntitiesComponent,
    EditEntityComponent,
    DeleteEntityComponent,
    ViewAccountEntitiesComponent,
    EditAccountEntityComponent,
    DeleteAccountEntityComponent,
    ViewAccountsComponent,
    DeleteAccountComponent,
    EditAccountComponent,
    CreateInvoiceComponent,
    ViewInvoicesComponent,
    DeleteInvoiceComponent,
    EditInvoiceComponent,
    DeleteEntityTypeComponent,
    CreateLicenseGroupComponent,
    ViewLicenseGroupsComponent,
    DeleteLicenseGroupComponent,
    EditLicenseGroupComponent,
    CreateLicenseEntityAllocationComponent,
    ViewLicenseEntityAllocationComponent,
    CreateLicenseConsumerGroupComponent,
    ViewLicenseConsumerGroupsComponent,
    EditLicenseEntityAllocationComponent,
    DeleteLicenseEntityAllocationComponent,
    CreateAccountEntityUserComponent,
    CreateRoleComponent,
    ViewRolesComponent,
    EditRoleComponent,
    DeleteRoleComponent,
    CreateUserRoleComponent,
    CreateUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatBadgeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
