import { Component, OnInit } from '@angular/core';
import { IAccount} from "../acct-manage/IAccount";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.sass']
})
export class AccountComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  getAccount(AccountID: number): any {
    return [
      {
        CustomerID: 100,
        AccountName: 'Arlington ISD',
        OrganizationName: 'Arlington Independent School District',
        OrganizationType: 'District',
        ProductType: [
          'AP', 'ACT'
        ],
        Country: 'United States',
        State: 'Texas',
        Region: 'Texas Region 1',
        District: 'Arlington Independent School District',
        DistrictId: 1
      }
      ]
  }
}
