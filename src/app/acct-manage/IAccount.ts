export interface IAccount {
  CustomerID: number,
  AccountName: string,
  OrganizationName: string,
  OrganizationType: string,
  ProductType: [],
  Country: string,
  State: string,
  Region: string,
  District: string,
  DistrictId: string
}
