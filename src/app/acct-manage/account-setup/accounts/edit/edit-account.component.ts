import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { IAccountDB} from "../../IAccountDB";
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.sass']
})
export class EditAccountComponent implements OnInit, OnDestroy {

  PageTitle: string = 'Edit Account';
  id: number;
  private sub: any;
  account: IAccountDB[] = [];

  // form controls for form fields
  accountId = new FormControl(this.account['accountId'], [Validators.required]);
  name = new FormControl(this.account['name']);
  entityId = new FormControl(this.account['entityId']);
  data: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getAccount();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // function that returns error message
  getErrorMessage(): string {
    return this.accountId.hasError('required') ? 'You must enter a value' : '';
  }

  editAccount() {
    // For some reason default form values are not being returned when submitting the form.
    // Get them from entityType if form value is empty.
    let name = +this.account['name'];
    if (this.name.value) {
      name = this.name.value;
    }
    let entityId = +this.account['entityId'];

    let body = {
      "accountId": this.id,
      "name": name,
      "verticalId": 0,
      "entityId": entityId,
      "entity": null
    };
    try {
      this.ApiService.editAccount(body)
        .subscribe(resp => {
            this.router.navigate(['/account-setup/accounts/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccount()  {
    try {
      this.ApiService.getAccount(this.id)
        .subscribe(account => {
            this.account = account
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
