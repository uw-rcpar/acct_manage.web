import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import { IEntity} from "../../IEntity";

@Component({
  selector: 'app-view-accounts',
  templateUrl: './view-accounts.component.html',
  styleUrls: ['./view-accounts.component.sass']
})
export class ViewAccountsComponent implements OnInit {

  PageTitle: string = 'View Accounts';
  AccountsData: any;
  data: IEntity[] = [];
  displayedColumns: string[] = ['AccountId', 'Name', 'VerticalId', 'EntityId', 'Actions'];

  constructor(private ApiService: ApiService) { }

  ngOnInit() {
    this.AccountsData = new MatTableDataSource();
    this.getAccounts();
  }

  getAccounts()  {
    this.ApiService.getAccounts().subscribe(data => this.AccountsData.data = data)
  }

}
