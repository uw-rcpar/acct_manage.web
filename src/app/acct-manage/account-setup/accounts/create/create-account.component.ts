import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.sass']
})
export class CreateAccountComponent implements OnInit {

  PageTitle: string = 'Create Account';
  // form controls for form fields
  name = new FormControl('', [Validators.required]);
  data: any;

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
  }

  // function that returns error message
  getErrorMessage():string {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  addAccount() {
    let body = {
      "name": this.name.value,
      "verticalId": 0
    };
    try {
      this.ApiService.createAccount(JSON.stringify(body))
        .subscribe(resp => {
            this.router.navigate(['/account-setup/accounts/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
