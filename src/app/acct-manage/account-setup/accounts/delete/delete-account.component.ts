import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { IAccountDB} from "../../IAccountDB";
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-delete-account',
  templateUrl: './delete-account.component.html',
  styleUrls: ['./delete-account.component.sass']
})
export class DeleteAccountComponent implements OnInit {

  PageTitle: string = 'Delete Account';
  id: number;
  private sub: any;
  data: any;
  account: IAccountDB[] = [];

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getAccount();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getAccount() {
    try {
      this.ApiService.getAccount(this.id)
        .subscribe(account => {
            this.account = account;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  deleteAccount() {
    try {
      this.ApiService.deleteAccount(this.id)
        .subscribe(entity => {
            this.router.navigate(['/account-setup/accounts/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
