import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import {IInvoice} from "../../IInvoice";

@Component({
  selector: 'app-delete-invoice',
  templateUrl: './delete-invoice.component.html',
  styleUrls: ['./delete-invoice.component.sass']
})
export class DeleteInvoiceComponent implements OnInit {

  PageTitle: string = 'Delete Invoice';
  id: number;
  private sub: any;
  data: any;
  invoice: IInvoice[] = [];

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getEntity();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getEntity()  {
    try {
      this.ApiService.getInvoice(this.id)
        .subscribe(invoice => {
            console.log(invoice, "invoice");
            this.invoice = invoice
            console.log(this.invoice, "this.invoice");
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  deleteInvoice() {
    try {
      this.ApiService.deleteInvoice(this.id)
        .subscribe(invoice => {
            this.router.navigate(['/account-setup/invoices/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}


