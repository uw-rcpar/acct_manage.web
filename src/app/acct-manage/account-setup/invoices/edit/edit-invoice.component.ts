import {Component, OnDestroy, OnInit} from '@angular/core';
import { IInvoice} from "../../IInvoice";
import { ApiService } from '../../../../api.service';
import {FormControl, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {IAccountDB} from "../../IAccountDB";

@Component({
  selector: 'app-edit-invoice',
  templateUrl: './edit-invoice.component.html',
  styleUrls: ['./edit-invoice.component.sass']
})
export class EditInvoiceComponent implements OnInit, OnDestroy {

  PageTitle: string = 'Edit Invoice';
  id: number;
  private sub: any;
  invoice: IInvoice[] = [];
  selectedAccount: any;
  accounts: IAccountDB[] = [];
  date: Date;
  // form controls for form fields
  accountId = new FormControl(this.invoice['accountId']);
  saleAmount = new FormControl(this.invoice['saleAmount']);
  contractDate = new FormControl(this.date);
  salesLeadId = new FormControl(this.invoice['salesLeadId']);
  data: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getInvoice();
    this.getAccounts();
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  editInvoice() {
    // For some reason default form values are not being returned when submitting the form.
    // Get them from licenseGroup if form value is empty.
    let cDate = this.invoice['contractDate']; // unix time stamp
    if (this.contractDate.value) {
      cDate = this.contractDate.value/1000|0; // convert to unix timestamp TODO verify this
    }
    let slid = this.invoice['salesLeadId']; // unix time stamp
    if (this.salesLeadId.value) {
      slid = this.salesLeadId.value;
    }
    let sa = this.invoice['saleAmount']; // unix time stamp
    if (this.saleAmount.value) {
      sa = this.saleAmount.value;
    }
    let body = {
      "invoiceId": this.id,
      "accountId": this.accountId.value,
      "saleAmount": +sa,
      "contractDate": cDate,
      "salesLeadId": +slid
    };
    console.log(body);
    try {
      this.ApiService.editInvoice(body)
        .subscribe(resp => {
            this.router.navigate(['/account-setup/invoices/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getInvoice()  {
    try {
      this.ApiService.getInvoice(this.id)
        .subscribe(invoice => {
            this.invoice = invoice;
            this.date = new Date(this.invoice['contractDate']*1000);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccounts()  {
    this.ApiService.getAccounts().subscribe(data => this.accounts = data)
  }

  setAccount(id): void {
    this.selectedAccount = id;
  }

}
