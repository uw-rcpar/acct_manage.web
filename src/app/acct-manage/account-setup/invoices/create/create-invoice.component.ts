import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import {IAccountDB} from "../../IAccountDB";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.sass']
})
export class CreateInvoiceComponent implements OnInit {

  PageTitle: string = 'Create Invoice';
  // form controls for form fields
  name = new FormControl('', [Validators.required]);
  accountId = new FormControl('', [Validators.required]);
  saleAmount = new FormControl('', [Validators.required]);
  contractDate = new FormControl(new  Date());
  salesLeadId = new FormControl('');
  data: any;
  accounts: IAccountDB[] = [];
  selectedAccount: any;

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
    this.getAccounts();
  }

  // function that returns error message
  getErrorMessage(): string {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  createInvoice() {
    let body = {
      "accountId": this.selectedAccount,
      "saleAmount": +this.saleAmount.value,
      "contractDate": this.contractDate.value/1000|0, // convert to unix timestamp TODO verify this
      "salesLeadId": +this.salesLeadId.value
    };
    console.log(body);
    try {
      this.ApiService.createInvoice(JSON.stringify(body))
        .subscribe(resp => {
            this.router.navigate(['/account-setup/invoices/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccounts()  {
    this.ApiService.getAccounts().subscribe(data => this.accounts = data)
  }

  setAccount(id): void {
    this.selectedAccount = id;
  }

}

