import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import { IInvoice} from "../../IInvoice";

@Component({
  selector: 'app-view-invoices',
  templateUrl: './view-invoices.component.html',
  styleUrls: ['./view-invoices.component.sass']
})
export class ViewInvoicesComponent implements OnInit {

  PageTitle: string = 'View Invoices';
  InvoicesData: any;
  data: IInvoice[] = [];
  displayedColumns: string[] = ['InvoiceId', 'AccountId', 'SaleAmount', 'ContractDate', 'SalesLeadId', 'Actions'];

  constructor(private ApiService: ApiService) { }

  ngOnInit() {
    this.InvoicesData = new MatTableDataSource();
    this.getInvoices();
  }

  getInvoices()  {
    this.ApiService.getInvoices().subscribe(data => this.InvoicesData.data = data)
  }

}
