export interface ILicenseConsumerGroup {
  licenseConsumerGroupId: number,
  licenseEntityAllocationId: number,
  accountEntityId: number,
  name: string
}
