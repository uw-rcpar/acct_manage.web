import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import { ILicenseConsumerGroup} from "../../ILicenseConsumerGroup";

@Component({
  selector: 'app-view-license-consumer-groups',
  templateUrl: './view-license-consumer-groups.component.html',
  styleUrls: ['./view-license-consumer-groups.component.sass']
})
export class ViewLicenseConsumerGroupsComponent implements OnInit {

  PageTitle: string = 'View License Consumer Groups';
  LicenseConsumerGroupsData: any;
  data: ILicenseConsumerGroup[] = [];
  displayedColumns: string[] = ['LicenseConsumerGroupId', 'Name', 'LicenseEntityAllocationId', 'AccountEntityId', 'Actions'];

  constructor(private ApiService: ApiService) { }

  ngOnInit() {
    this.LicenseConsumerGroupsData = new MatTableDataSource();
    this.getLicenseGroups();
  }

  public getLicenseGroups()  {
    this.ApiService.getLicenseConsumerGroups().subscribe(data => this.LicenseConsumerGroupsData.data = data)
  }

}
