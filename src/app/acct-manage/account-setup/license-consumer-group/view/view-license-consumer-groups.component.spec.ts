import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLicenseConsumerGroupsComponent } from './view-license-consumer-groups.component';

describe('ViewLicenseConsumerGroupsComponent', () => {
  let component: ViewLicenseConsumerGroupsComponent;
  let fixture: ComponentFixture<ViewLicenseConsumerGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLicenseConsumerGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLicenseConsumerGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
