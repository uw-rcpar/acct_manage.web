import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLicenseConsumerGroupComponent } from './create-license-consumer-group.component';

describe('CreateLicenseConsumerGroupComponent', () => {
  let component: CreateLicenseConsumerGroupComponent;
  let fixture: ComponentFixture<CreateLicenseConsumerGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLicenseConsumerGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLicenseConsumerGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
