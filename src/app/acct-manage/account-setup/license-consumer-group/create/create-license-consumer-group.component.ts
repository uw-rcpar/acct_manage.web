import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-license-consumer-group',
  templateUrl: './create-license-consumer-group.component.html',
  styleUrls: ['./create-license-consumer-group.component.sass']
})
export class CreateLicenseConsumerGroupComponent implements OnInit {

  PageTitle: string = 'Create License Consumer Group (Classroom)';
  // form controls for form fields
  licenseEntityAllocationId = new FormControl('');
  accountEntityId = new FormControl('');
  name = new FormControl('');
  data: any;

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
  }

  createLicenseConsumerGroup() {
    let body = {
      "licenseEntityAllocationId": +this.licenseEntityAllocationId.value,
      "accountEntityId": +this.accountEntityId.value,
      "name": this.name.value,
    };
    console.log(body);
    try {
      this.ApiService.createLicenseConsumerGroup(JSON.stringify(body))
        .subscribe(resp => {
            this.router.navigate(['/account-setup/license-consumer-groups/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
