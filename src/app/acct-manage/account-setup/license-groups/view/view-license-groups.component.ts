import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import { ILicenseGroup} from "../../ILicenseGroup";

@Component({
  selector: 'app-view-license-groups',
  templateUrl: './view-license-groups.component.html',
  styleUrls: ['./view-license-groups.component.sass']
})
export class ViewLicenseGroupsComponent implements OnInit {

  PageTitle: string = 'View Invoices';
  LicenseGroupsData: any;
  data: ILicenseGroup[] = [];
  displayedColumns: string[] = ['LicenseGroupId', 'InvoiceId', 'AccountId', 'ProductId', 'Quantity', 'Duration', 'StartDate', 'EndDate', 'Actions'];

  constructor(private ApiService: ApiService) { }

  ngOnInit() {
    this.LicenseGroupsData = new MatTableDataSource();
    this.getLicenseGroups();
  }

  getLicenseGroups()  {
    this.ApiService.getLicenseGroups().subscribe(data => this.LicenseGroupsData.data = data)
  }
}
