import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLicenseGroupsComponent } from './view-license-groups.component';

describe('ViewLicenseGroupsComponent', () => {
  let component: ViewLicenseGroupsComponent;
  let fixture: ComponentFixture<ViewLicenseGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLicenseGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLicenseGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
