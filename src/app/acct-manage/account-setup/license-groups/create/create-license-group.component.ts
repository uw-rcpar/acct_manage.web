import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import { IInvoice} from "../../IInvoice";
import { Router } from "@angular/router";
import {IAccountDB} from "../../IAccountDB";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-create-license-group',
  templateUrl: './create-license-group.component.html',
  styleUrls: ['./create-license-group.component.sass']
})
export class CreateLicenseGroupComponent implements OnInit {

  PageTitle: string = 'Create License Group';
  // form controls for form fields
  invoiceId = new FormControl('');
  accountId = new FormControl('');
  productId = new FormControl('');
  quantity = new FormControl('');
  startDate = new FormControl(new Date());
  endDate = new FormControl(new Date());
  data: any;
  invoices: IInvoice[] = [];
  accounts: IAccountDB[] = [];
  selectedInvoice: any;
  selectedAccount: any;
  invoiceList: any;

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
    this.getInvoices();
    this.getAccounts();
  }

  createLicenseGroup() {
    let body = {
      "invoiceId": this.selectedInvoice,
      "accountId": +this.accountId.value,
      "productId": +this.productId.value,
      "quantity": +this.quantity.value,
      "duration": this.getDuration(this.startDate.value, this.endDate.value), // TODO verify this
      "startDate": this.startDate.value/1000|0, // convert to unix timestamp TODO verify this
      "endDate": this.endDate.value/1000|0, // convert to unix timestamp TODO verify this
    };
    console.log(body);
    try {
      this.ApiService.createLicenseGroup(JSON.stringify(body))
        .subscribe(resp => {
            this.router.navigate(['/account-setup/license-groups/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getInvoices()  {
    this.ApiService.getInvoices().subscribe(data => this.invoices = data);
  }

  getAccounts()  {
    this.ApiService.getAccounts().subscribe(data => this.accounts = data);
  }

  setInvoice(id): void {
    this.selectedInvoice = id;
  }

  accountChosen(id: number): void {
    this.selectedAccount = id;
    this.invoiceList = this.getInvoicesFromAccount(id);
  }

  getInvoicesFromAccount(id: number) {
    this.ApiService.getInvoices()
        .pipe(map(data => data.filter(invoice => invoice.accountId == id)))
        .subscribe(data => this.invoices = data);
  }

  getDuration(startDate: number, endDate: number): number {
    let start = new Date(this.startDate.value);
    let end = new Date(this.endDate.value);
    let difference = end.getTime() - start.getTime();
    let oneDay = 1000*60*60*24;
    return Math.round(difference/oneDay);
  }

}


