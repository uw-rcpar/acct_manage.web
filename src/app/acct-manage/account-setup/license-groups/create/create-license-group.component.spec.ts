import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLicenseGroupComponent } from './create-license-group.component';

describe('CreateLicenseGroupComponent', () => {
  let component: CreateLicenseGroupComponent;
  let fixture: ComponentFixture<CreateLicenseGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLicenseGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLicenseGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
