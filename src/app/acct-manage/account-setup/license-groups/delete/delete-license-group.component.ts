import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import {ILicenseGroup} from "../../ILicenseGroup";

@Component({
  selector: 'app-delete-license-group',
  templateUrl: './delete-license-group.component.html',
  styleUrls: ['./delete-license-group.component.sass']
})
export class DeleteLicenseGroupComponent implements OnInit {

  PageTitle: string = 'Delete License Group';
  id: number;
  private sub: any;
  data: any;
  licenseGroup: ILicenseGroup[] = [];

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getLicenseGroup();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getLicenseGroup()  {
    try {
      this.ApiService.getLicenseGroup(this.id)
        .subscribe(resp => {
            console.log(resp, "resp");
            this.licenseGroup = resp
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  deleteLicenseGroup() {
    try {
      this.ApiService.deleteLicenseGroup(this.id)
        .subscribe(invoice => {
            this.router.navigate(['/account-setup/license-groups/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
