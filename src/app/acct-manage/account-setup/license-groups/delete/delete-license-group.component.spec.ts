import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteLicenseGroupComponent } from './delete-license-group.component';

describe('DeleteLicenseGroupComponent', () => {
  let component: DeleteLicenseGroupComponent;
  let fixture: ComponentFixture<DeleteLicenseGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteLicenseGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteLicenseGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
