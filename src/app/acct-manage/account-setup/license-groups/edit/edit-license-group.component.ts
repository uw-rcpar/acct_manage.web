import {Component, OnDestroy, OnInit} from '@angular/core';
import { IInvoice} from "../../IInvoice";
import { ApiService } from '../../../../api.service';
import {FormControl, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {IAccountDB} from "../../IAccountDB";
import {ILicenseGroup} from "../../ILicenseGroup";
import {map} from "rxjs/operators";


@Component({
  selector: 'app-edit-license-group',
  templateUrl: './edit-license-group.component.html',
  styleUrls: ['./edit-license-group.component.sass']
})
export class EditLicenseGroupComponent implements OnInit {

  PageTitle: string = 'Edit License Group';
  id: number;
  private sub: any;
  licenseGroup: ILicenseGroup[] = [];
  selectedAccount: any;
  selectedInvoice: any;
  accounts: IAccountDB[] = [];
  invoices: IInvoice[] =[];
  dateStart: Date;
  dateEnd: Date;
  // form controls for form fields
  accountId = new FormControl(this.licenseGroup['accountId']);
  invoiceId = new FormControl(this.licenseGroup['invoiceId']);
  productId = new FormControl(this.licenseGroup['productId']);
  quantity = new FormControl(this.licenseGroup['quantity']);
  startDate = new FormControl(this.dateStart);
  endDate = new FormControl(this.dateEnd);
  invoiceList: any;
  data: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getLicenseGroup();
    this.getAccounts();
    this.getInvoices();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  editLicenseGroup() {
    // For some reason default form values are not being returned when submitting the form.
    // Get them from licenseGroup if form value is empty.
    let startingDate = this.licenseGroup['startDate']; // unix time stamp
    if (this.startDate.value) {
      startingDate = this.startDate.value/1000|0; // convert to unix timestamp TODO verify this
    }
    let endingDate = this.licenseGroup['endDate'];// unix time stamp
    if (this.endDate.value) {
      endingDate = this.endDate.value/1000|0; // convert to unix timestamp TODO verify this
    }
    let product = this.licenseGroup['productId'];
    if (this.productId.value) {
      product = +this.productId.value;
    }
    let quant = this.licenseGroup['quantity'];
    if (this.quantity.value) {
      quant = +this.quantity.value;
    }

    let body = {
      "licenseGroupId": this.id,
      "invoiceId": +this.invoiceId.value,
      "accountId": +this.accountId.value,
      "productId": product,
      "quantity": quant,
      "duration": this.getDuration(new Date(startingDate*1000), new Date(endingDate*1000)),
      "startDate": startingDate,
      "endDate": endingDate
    };
    console.log(body);
    try {
      this.ApiService.editLicenseGroup(body)
        .subscribe(resp => {
            this.router.navigate(['/account-setup/license-groups/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getLicenseGroup()  {
    try {
      this.ApiService.getLicenseGroup(this.id)
        .subscribe(resp => {
            this.licenseGroup = resp;
            this.dateStart = new Date(this.licenseGroup['startDate']*1000);
            this.dateEnd = new Date(this.licenseGroup['endDate']*1000);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccounts()  {
    this.ApiService.getAccounts().subscribe(data => this.accounts = data)
  }

  getInvoices()  {
    this.ApiService.getInvoices().subscribe(data => this.invoices = data)
  }

  setInvoice(id): void {
    this.selectedInvoice = id;
  }

  accountChosen(id: number): void {
    this.selectedAccount = id;
    this.invoiceList = this.getInvoicesFromAccount(id);
  }

  getInvoicesFromAccount(id: number): any {
    this.ApiService.getInvoices()
      .pipe(map(data => data.filter(invoice => invoice.accountId == id)))
      .subscribe(data => this.invoices = data);
  }

  getDuration(startDate: Date, endDate: Date): number {
    let difference = endDate.getTime() - startDate.getTime();
    let oneDay = 1000*60*60*24;
    return Math.round(difference/oneDay);
  }

}
