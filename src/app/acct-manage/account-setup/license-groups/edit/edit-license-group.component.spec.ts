import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLicenseGroupComponent } from './edit-license-group.component';

describe('EditLicenseGroupComponent', () => {
  let component: EditLicenseGroupComponent;
  let fixture: ComponentFixture<EditLicenseGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLicenseGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLicenseGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
