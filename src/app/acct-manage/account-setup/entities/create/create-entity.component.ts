import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import {IEntity} from "../../IEntity";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-entity',
  templateUrl: './create-entity.component.html',
  styleUrls: ['./create-entity.component.sass']
})
export class CreateEntityComponent implements OnInit {

  PageTitle: string = 'Create Entity';
  // form controls for form fields
  name = new FormControl('', [Validators.required]);
  entityTypeId = new FormControl('', [Validators.required]);
  parentEntityId = new FormControl('', [Validators.required]);
  parentId: number = 0;
  data: any;
  entities: IEntity[];

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {

  }

  // function that returns error message
  getErrorMessage(): string {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  addEntity() {
    if (this.parentEntityId.value) {
      this.parentId = +this.parentEntityId.value;
    }
    let body = {
      "name": this.name.value,
      "entityTypeId": +this.entityTypeId.value,
      "parentEntityId": this.parentId
    };
    try {
      this.ApiService.createEntity(JSON.stringify(body))
        .subscribe(resp => {
            this.router.navigate(['/account-setup/entities/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
