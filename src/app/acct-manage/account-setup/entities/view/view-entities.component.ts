import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import { IEntity} from "../../IEntity";

@Component({
  selector: 'app-view-entities',
  templateUrl: './view-entities.component.html',
  styleUrls: ['./view-entities.component.sass']
})
export class ViewEntitiesComponent implements OnInit {

  PageTitle: string = 'View Entities';
  EntitiesData: any;
  data: IEntity[] = [];
  displayedColumns: string[] = ['EntityId', 'Name', 'ParentEntityId', 'Actions'];

  constructor(private ApiService: ApiService) { }

  ngOnInit() {
    this.EntitiesData = new MatTableDataSource();
    this.getEntities();
  }

  public getEntities()  {
    this.ApiService.getEntities().subscribe(data => this.EntitiesData.data = data)
  }

}
