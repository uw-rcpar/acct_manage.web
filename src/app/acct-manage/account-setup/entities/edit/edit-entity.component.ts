import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { IEntity} from "../../IEntity";
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-entity',
  templateUrl: './edit-entity.component.html',
  styleUrls: ['./edit-entity.component.sass']
})
export class EditEntityComponent implements OnInit, OnDestroy {

  PageTitle: string = 'Edit Entity';
  id: number;
  private sub: any;
  entity: IEntity[] = [];
  // form controls for form fields
  name = new FormControl(this.entity['name'], [Validators.required]);
  parentEntityId = new FormControl(this.entity['parentEntityId']);
  data: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getEntity();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // function that returns error message
  getErrorMessage() {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  editEntity() {
    // For some reason default form values are not being returned when submitting the form.
    // Get them from entityType if form value is empty.
    let name = this.entity['name'];
    if (this.name.value) {
      name = this.name.value;
    }
    console.log(name);
    let parentEntityId = +this.entity['parentEntityTypeId'];
    if (this.parentEntityId.value) {
      parentEntityId = +this.parentEntityId.value;
    }

    let body = {
      "entityId": this.id,
      "name": name,
      "parentEntityId": parentEntityId
    };

    try {
      this.ApiService.editEntity(body)
        .subscribe(resp => {
            this.router.navigate(['/account-setup/entities/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getEntity()  {
    try {
      this.ApiService.getEntity(this.id)
        .subscribe(entity => {
            this.entity = entity
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
