import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { IEntity} from "../../IEntity";
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-delete-entity',
  templateUrl: './delete-entity.component.html',
  styleUrls: ['./delete-entity.component.sass']
})
export class DeleteEntityComponent implements OnInit {

  PageTitle: string = 'Delete Entity';
  id: number;
  private sub: any;
  data: any;
  entity: IEntity[] = [];

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getEntity();
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getEntity()  {
    try {
      this.ApiService.getEntity(this.id)
        .subscribe(entity => {
            console.log(entity, "entity");
            this.entity = entity
            console.log(this.entity, "this.entity");
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  deleteEntity() {
    try {
      this.ApiService.deleteEntity(this.id)
        .subscribe(entity => {
            this.router.navigate(['/account-setup/entities/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
