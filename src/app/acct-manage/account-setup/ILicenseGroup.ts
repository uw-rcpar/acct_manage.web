export interface ILicenseGroup {
  licenseGroupId: number,
  invoiceId: number,
  saleAmount: number,
  contractDate: number,
  salesLeadId: number
}
