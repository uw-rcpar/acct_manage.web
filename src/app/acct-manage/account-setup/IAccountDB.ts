export interface IAccountDB {
  accountId: number,
  name: string,
  verticalId: number,
  entityId: number
}
