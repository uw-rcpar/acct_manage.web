import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAccountEntityComponent } from './edit-account-entity.component';

describe('EditAccountEntityComponent', () => {
  let component: EditAccountEntityComponent;
  let fixture: ComponentFixture<EditAccountEntityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAccountEntityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAccountEntityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
