import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { IAccountEntity} from "../../IAccountEntity";
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from "rxjs/operators";
import { IAccountDB } from "../../IAccountDB";
import { IEntity } from "../../IEntity";

@Component({
  selector: 'app-edit-account-entity',
  templateUrl: './edit-account-entity.component.html',
  styleUrls: ['./edit-account-entity.component.sass']
})
export class EditAccountEntityComponent implements OnInit, OnDestroy {


  PageTitle: string = 'Edit AccountEntity';
  id: number;
  private sub: any;
  accountEntity: IAccountEntity[] = [];
  accounts: IAccountDB[] = [];
  entities: IEntity[] = [];
  account: any;
  entity: any;
  selectedAccount: any;
  selectedEntity: any;
  accountEntityLoading: boolean = true;
  // form controls for form fields
  accountId = new FormControl(this.selectedAccount);
  entityId = new FormControl(this.selectedEntity);
  data: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getAccountEntity();
    this.getAccounts();
    this.getEntities();
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // function that returns error message
  getErrorMessage(): string {
    return this.accountId.hasError('required') ? 'You must enter a value' : '';
  }

  editAccountEntity() {
    // For some reason default form values are not being returned when submitting the form.
    // Get them from entityType if form value is empty.
    let accountId = +this.accountEntity['accountId'];
    if (this.accountId.value) {
      accountId = +this.accountId.value;
    }
    let entityId = +this.accountEntity['entityId'];
    if (this.entityId.value) {
      entityId = +this.entityId.value;
    }

    let body = {
      "accountEntityId": this.id,
      "accountId": accountId,
      "entityId": entityId
    };
    try {
      this.ApiService.editAccountEntity(body)
        .subscribe(resp => {
            this.router.navigate(['/account-setup/account-entities/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccountEntity()  {
    try {
      this.ApiService.getAccountEntity(this.id)
        .pipe(
        finalize(() => {
          this.accountEntityLoading = false;
        })
      )
        .subscribe(accountEntity => {
            this.accountEntity = accountEntity;
            // set selected account and entity here
            this.selectedAccount = this.accountEntity['accountId'];
            this.selectedEntity = this.accountEntity['entityId'];
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccounts() {
    try {
      this.ApiService.getAccounts()
        .subscribe(resp => {
            this.accounts = resp;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccount(id) {
    try {
      this.ApiService.getAccount(id)
        .subscribe(resp => {
            this.account = resp;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getEntities() {
    try {
      this.ApiService.getEntities()
        .subscribe(resp => {
            this.entities = resp;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getEntity(id) {
    try {
      this.ApiService.getEntity(id)
        .subscribe(resp => {
            this.entity = resp;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  setAccount(id) {
    this.selectedAccount = id;
   // this.getAccount(id);
  }

  setEntity(id) {
    this.selectedEntity = id;
   // this.getEntity(id);
  }


}
