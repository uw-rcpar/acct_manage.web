import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import { IAccountEntity } from "../../IAccountEntity";
import {pipe, forkJoin, Observable } from "rxjs";
import {mergeMap, map, switchMap, tap} from "rxjs/operators";


@Component({
  selector: 'app-view-account-entities',
  templateUrl: './view-account-entities.component.html',
  styleUrls: ['./view-account-entities.component.sass']
})
export class ViewAccountEntitiesComponent implements OnInit {

  PageTitle: string = 'View Account Entities';
  AccountEntitiesData: any;
  AccountEntities: any;
  data: IAccountEntity[] = [];
  displayedColumns: string[] = ['AccountEntityId', 'AccountId', 'EntityId', 'Actions'];
  accounts: any;
  entities: any;

  newData: any;
  newDataNew: [];

  constructor(private ApiService: ApiService) { }

  ngOnInit() {
    this.AccountEntitiesData = new MatTableDataSource();
    this.getAccountEntities();
    this.getAccounts();
    //this.getJoined();
    //this.getAccountNameFromID(3);

  }

  getAccountEntities(): any  {
    this.ApiService.getAccountEntities()
      .subscribe(data => {
        this.AccountEntitiesData.data = data
      });
  }

  getAccounts():any {
    this.ApiService.getAccounts().subscribe(data => this.accounts = data)
  }

  getEntities():any {
    this.ApiService.getEntities().subscribe(data => this.entities = data)
  }

  /*accountName: string = "";
  getAccountNameFromID(id: number) {
    this.ApiService.getAccounts()
      .pipe(map(data => data.filter(account => account.accountId == id)))
      .subscribe(data => this.accountName = data[0]['name']);
    console.log(this.accountName, 'account name in get function');
    return this.accountName;
  }
*/
  /* map test
  getJoined(): any {
    this.ApiService.getAccounts()
      .pipe(map(data => data['accountId'] == this.AccountEntitiesData['accountId']))
      .subscribe(data => this.newData = data)
  }*/
/*
  getJoined() {

    let aes = this.ApiService.getAccountEntities();
    let as = this.ApiService.getAccounts();
    let es = this.ApiService.getEntities();

    forkJoin([aes, as, es]).subscribe(results => {
      console.log(results);
      for (var key in results[0]) {
        console.log(results[0][key].accountId, 'results[0][key].accountId');
        console.log(this.getAccountNameFromID(results[0][key].accountId), 'get name in forkjoin');'' +
        console.log(this.accountName, "this accountName");
        //this.newDataNew.[] = this.getAccountNameFromID(results[0][key].accountId);
        //console.log(this.newDataNew);
      }
      for (var key in results[1]) {
        //console.log(results[1][key]);
      }
      for (var key in results[2]) {
        //console.log(results[2][key]);
      }
    });
  }*/
}
