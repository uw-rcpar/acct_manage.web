import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAccountEntitiesComponent } from './view-account-entities.component';

describe('ViewAccountEntitiesComponent', () => {
  let component: ViewAccountEntitiesComponent;
  let fixture: ComponentFixture<ViewAccountEntitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAccountEntitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAccountEntitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
