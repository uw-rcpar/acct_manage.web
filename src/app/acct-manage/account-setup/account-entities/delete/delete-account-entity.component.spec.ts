import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteAccountEntityComponent } from './delete-account-entity.component';

describe('DeleteAccountEntityComponent', () => {
  let component: DeleteAccountEntityComponent;
  let fixture: ComponentFixture<DeleteAccountEntityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteAccountEntityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteAccountEntityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
