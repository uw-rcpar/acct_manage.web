import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { IEntity} from "../../IEntity";
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import {IAccountEntity} from "../../IAccountEntity";
import { BehaviorSubject} from "rxjs";
import {finalize} from "rxjs/operators";


@Component({
  selector: 'app-delete-account-entity',
  templateUrl: './delete-account-entity.component.html',
  styleUrls: ['./delete-account-entity.component.sass']
})
export class DeleteAccountEntityComponent implements OnInit {

  PageTitle: string = 'Delete AccountEntity';
  id: number;
  private sub: any;
  data: any;
  accountEntity: IAccountEntity[] = [];
  account: any;
  entity: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getAccountEntity();
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getAccountEntity()  {
    try {
      this.ApiService.getAccountEntity(this.id)
        .subscribe(accountEntity => {
            this.accountEntity = accountEntity;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  deleteAccountEntity() {
    try {
      this.ApiService.deleteAccountEntity(this.id)
        .subscribe(accountEntity => {
            this.router.navigate(['/account-setup/account-entities/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
