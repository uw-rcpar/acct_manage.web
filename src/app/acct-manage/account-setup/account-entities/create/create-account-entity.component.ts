import {Component, OnInit, AfterViewInit, Pipe} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import { IEntity } from "../../IEntity";
import { IAccountDB } from "../../IAccountDB";
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from "rxjs/operators";
import {Observable} from "rxjs";
import {IAccountEntity} from "../../IAccountEntity";

@Component({
  selector: 'app-create-account-entity',
  templateUrl: './create-account-entity.component.html',
  styleUrls: ['./create-account-entity.component.sass']
})
export class CreateAccountEntityComponent implements OnInit, AfterViewInit {

  PageTitle: string = 'Create Account Entity';
  // form controls for form fields
  accountId = new FormControl(null );
  entityId = new FormControl(null );
  accounts: IAccountDB[] = [];
  entities: IEntity[] = [];
  selectedAccount: any;
  selectedEntity: any;
  account: any;
  entity: any;
  data: any;
  accountLoading: boolean = true;
  entityLoading: boolean = true;

  constructor( private ApiService: ApiService, private route: ActivatedRoute, private router: Router ) { }

  ngOnInit() {
    this.getAccounts();
    this.getEntities();
  }

  ngAfterViewInit() {
  }

  // function that returns error message
  getErrorMessage(): string {
    return this.accountId.hasError('required') ? 'You must enter a value' : '';
  }

  addAccountEntity() {

    this.updateAccount();

    // create the account entity
    let body = {
      "accountId": this.accountId.value,
      "entityId" : this.entityId.value
    };

    try {
      this.ApiService.createAccountEntity(JSON.stringify(body))
        .subscribe(resp => {
            console.log(resp, "resp acccount entity");
            this.router.navigate(['/account-setup/account-entities/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }

  }

  updateAccount() {
    // update the account with entity id
    let accountBody = {
      "accountId": this.selectedAccount,
      "name": this.account.name,
      "verticalId": 0,
      "entityId": this.entityId.value
    };
    try {
      this.ApiService.editAccount(accountBody)
        .subscribe(resp => {
            this.data = resp;
            console.log(resp, "edit account resp")
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccounts() {
    try {
      this.ApiService.getAccounts()
        .subscribe(resp => {
            this.accounts = resp;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getAccount() {
    try {
      this.ApiService.getAccount(this.selectedAccount)
        .pipe(
          finalize(() => {
            this.accountLoading = false;
          })
        )
        .subscribe(resp => {
            this.account = resp;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getEntities() {
    try {
      this.ApiService.getEntities()
        .subscribe(resp => {
            this.entities = resp;
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getEntity() {
    try {
      this.ApiService.getEntity(this.selectedEntity)
        .pipe(
          finalize(() => {
            this.entityLoading = false;
          })
        )
        .subscribe(resp => {
            this.entity = resp;
            this.selectedEntity = this.entity['entityId'];
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  setAccount(id): void {
    this.selectedAccount = id;
    this.getAccount();
  }

  setEntity(id): void {
    this.selectedEntity = id;
    this.getEntity();
  }

}
