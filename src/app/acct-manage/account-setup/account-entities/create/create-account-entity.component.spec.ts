import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccountEntityComponent } from './create-account-entity.component';

describe('CreateAccountEntityComponent', () => {
  let component: CreateAccountEntityComponent;
  let fixture: ComponentFixture<CreateAccountEntityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAccountEntityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccountEntityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
