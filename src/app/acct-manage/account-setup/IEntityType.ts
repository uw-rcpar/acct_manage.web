export interface IEntityType {
  name: string;
  parentEntityTypeId: number
}
