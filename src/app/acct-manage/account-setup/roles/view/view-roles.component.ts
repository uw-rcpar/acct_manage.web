import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import { IRole} from "../../IRole";
import {IEntity} from "../../IEntity";

@Component({
  selector: 'app-view-roles',
  templateUrl: './view-roles.component.html',
  styleUrls: ['./view-roles.component.sass']
})
export class ViewRolesComponent implements OnInit {

  PageTitle: string = 'View Roles';
  rolesData: any;
  data: IRole[] = [];
  displayedColumns: string[] = ['RoleId', 'Name', 'Context', 'CreatedDate', 'UpdatedDate', 'Actions'];

  constructor(private ApiService: ApiService) { }

  ngOnInit() {
    this.rolesData = new MatTableDataSource();
    this.getRoles();
  }

  getRoles()  {
    this.ApiService.getRoles().subscribe(data => this.rolesData.data = data)
  }

}
