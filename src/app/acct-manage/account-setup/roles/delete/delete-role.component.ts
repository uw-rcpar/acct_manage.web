import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IRole} from "../../IRole";
import {ILicenseGroup} from "../../ILicenseGroup";


@Component({
  selector: 'app-delete-role',
  templateUrl: './delete-role.component.html',
  styleUrls: ['./delete-role.component.sass']
})
export class DeleteRoleComponent implements OnInit {


  PageTitle: string = 'Delete Role';
  id: number;
  private sub: any;
  data: any;
  role: IRole[] = [];

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getRole();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getRole()  {
    try {
      this.ApiService.getRole(this.id)
        .subscribe(resp => {
            console.log(resp, "resp");
            this.role = resp
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  deleteRole() {
    try {
      this.ApiService.deleteRole(this.id)
        .subscribe(invoice => {
            this.router.navigate(['/account-setup/roles/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
