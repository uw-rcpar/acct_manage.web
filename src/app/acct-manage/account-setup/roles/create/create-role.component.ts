import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-role',
  templateUrl: './create-role.component.html',
  styleUrls: ['./create-role.component.sass']
})
export class CreateRoleComponent implements OnInit {

  PageTitle: string = 'Create Role';
  // form controls for form fields
  name = new FormControl('', [Validators.required]);
  context = new FormControl('');

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
  }

  // function that returns error message
  getErrorMessage():string {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  addRole() {
    let body = [{
      "name": this.name.value,
      "context": +this.context.value,
      "createdDate": Math.floor(Date.now() / 1000), // convert to unix timestamp,
      "updatedDate": Math.floor(Date.now() / 1000) // convert to unix timestamp
    }];
    console.log(body);
    try {
      this.ApiService.createRole(JSON.stringify(body))
        .subscribe(resp => {
          console.log(resp);
            this.router.navigate(['/account-setup/roles/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
