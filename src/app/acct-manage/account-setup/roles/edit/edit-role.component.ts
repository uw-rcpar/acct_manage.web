import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { IRole} from "../../IRole";
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.sass']
})
export class EditRoleComponent implements OnInit {

  PageTitle: string = 'Edit Role';
  id: number;
  private sub: any;
  role: IRole[] = [];
  createdDate: number;

  // form controls for form fields
  name = new FormControl(this.role['name']);
  context = new FormControl(this.role['context']);
  data: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getRole();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  editRole() {
    // For some reason default form values are not being returned when submitting the form.
    // Get them from entityType if form value is empty.
    let n = +this.role['name'];
    if (this.name.value) {
      n = this.name.value;
    }
    let c = +this.role['context'];
    if (this.context.value) {
      c = this.context.value;
    }

    let body = {
      "roleId": this.id,
      "name": n,
      "context": c,
      "createdDate": this.createdDate,
      "updatedDate": Math.floor(Date.now() / 1000), // convert to unix timestamp,
    };
    console.log(body);
    try {
      this.ApiService.editRole(body)
        .subscribe(resp => {
            this.router.navigate(['/account-setup/roles/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getRole()  {
    try {
      this.ApiService.getRole(this.id)
        .subscribe(role => {
            this.role = role;
            this.createdDate = role['createdDate'];
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
