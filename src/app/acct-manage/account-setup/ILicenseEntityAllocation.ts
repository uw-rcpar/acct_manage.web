export interface ILicenseEntityAllocation {
  licenseEntityAllocationId: number,
  licenseGroupId: number,
  accountEntityId: number,
  quantity: number
}
