export interface IEntity {
  entityId: number,
  entityTypeId: number,
  name: string,
  parentEntityTypeId: number,
  parentEntityType: any
}
