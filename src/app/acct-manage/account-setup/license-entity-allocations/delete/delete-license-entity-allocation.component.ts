import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { ActivatedRoute, Router } from '@angular/router';
import {ILicenseEntityAllocation} from "../../ILicenseEntityAllocation";

@Component({
  selector: 'app-delete-license-entity-allocation',
  templateUrl: './delete-license-entity-allocation.component.html',
  styleUrls: ['./delete-license-entity-allocation.component.sass']
})
export class DeleteLicenseEntityAllocationComponent implements OnInit {

  PageTitle: string = 'Delete License Entity Allocation';
  id: number;
  private sub: any;
  data: any;
  licenseEntityAllocation: ILicenseEntityAllocation[] = [];

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getLicenseEntityAllocation();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getLicenseEntityAllocation()  {
    try {
      this.ApiService.getLicenseEntityAllocation(this.id)
        .subscribe(resp => {
            console.log(resp, "resp");
            this.licenseEntityAllocation = resp
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  deleteLicenseEntityAllocation() {
    try {
      this.ApiService.deleteLicenseEntityAllocation(this.id)
        .subscribe(invoice => {
            this.router.navigate(['/account-setup/license-entity-allocations/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }


}
