import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteLicenseEntityAllocationComponent } from './delete-license-entity-allocation.component';

describe('DeleteLicenseEntityAllocationComponent', () => {
  let component: DeleteLicenseEntityAllocationComponent;
  let fixture: ComponentFixture<DeleteLicenseEntityAllocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteLicenseEntityAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteLicenseEntityAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
