import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import {ILicenseEntityAllocation} from "../../ILicenseEntityAllocation";

@Component({
  selector: 'app-edit-license-entity-allocation',
  templateUrl: './edit-license-entity-allocation.component.html',
  styleUrls: ['./edit-license-entity-allocation.component.sass']
})

export class EditLicenseEntityAllocationComponent implements OnInit {

  PageTitle: string = 'Edit License Entity Allocation';
  id: number;
  private sub: any;
  // form controls for form fields
  licenseEntityAllocation: ILicenseEntityAllocation[] = [];
  licenseEntityAllocationId = new FormControl('');
  licenseGroupId = new FormControl('');
  accountEntityId = new FormControl('');
  quantity = new FormControl('');
  data: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getLicenseEntityAllocation();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getLicenseEntityAllocation()  {
    try {
      this.ApiService.getLicenseEntityAllocation(this.id)
        .subscribe(licenseEntityAllocation => {
            this.licenseEntityAllocation = licenseEntityAllocation;
            // set selected account and entity here
            //this.selectedAccount = this.accountEntity['accountId'];
            //this.selectedEntity = this.accountEntity['entityId'];
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  editLicenseEntityAllocation() {
    // For some reason default form values are not being returned when submitting the form.
    // Get them from entityType if form value is empty.
    let LGId = +this.licenseEntityAllocation['licenseGroupId'];
    if (this.licenseGroupId.value) {
      LGId = +this.licenseGroupId.value;
    }
    let aEI = +this.licenseEntityAllocation['accountEntityId'];
    if (this.accountEntityId.value) {
      aEI = +this.accountEntityId.value;
    }
    let quant = +this.licenseEntityAllocation['quantity'];
    if (this.quantity.value) {
      quant = +this.quantity.value;
    }

    let body = {
      "licenseEntityAllocationId": this.id,
      "licenseGroupId": LGId,
      "accountEntityId": aEI,
      "quantity": quant
    };
    try {
      this.ApiService.editLicenseEntityAllocation(body)
        .subscribe(resp => {
            this.router.navigate(['/account-setup/license-entity-allocations/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }
}
