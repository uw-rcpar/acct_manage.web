import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditLicenseEntityAllocationComponent } from './edit-license-entity-allocation.component';

describe('EditLicenseEntityAllocationComponent', () => {
  let component: EditLicenseEntityAllocationComponent;
  let fixture: ComponentFixture<EditLicenseEntityAllocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditLicenseEntityAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditLicenseEntityAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
