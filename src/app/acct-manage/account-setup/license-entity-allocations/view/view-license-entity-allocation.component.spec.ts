import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewLicenseEntityAllocationComponent } from './view-license-entity-allocation.component';

describe('ViewLicenseEntityAllocationComponent', () => {
  let component: ViewLicenseEntityAllocationComponent;
  let fixture: ComponentFixture<ViewLicenseEntityAllocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLicenseEntityAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewLicenseEntityAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
