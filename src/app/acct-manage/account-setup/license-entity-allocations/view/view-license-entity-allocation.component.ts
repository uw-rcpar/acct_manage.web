import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import { ILicenseEntityAllocation} from "../../ILicenseEntityAllocation";

@Component({
  selector: 'app-view-license-entity-allocation',
  templateUrl: './view-license-entity-allocation.component.html',
  styleUrls: ['./view-license-entity-allocation.component.sass']
})
export class ViewLicenseEntityAllocationComponent implements OnInit {

  PageTitle: string = 'View Entities';
  AllocationsData: any;
  data: ILicenseEntityAllocation[] = [];
  displayedColumns: string[] = ['LicenseEntityAllocationId', 'LicenseGroupId', 'AccountEntityId', 'Quantity', 'Actions'];

  constructor(private ApiService: ApiService) { }

  ngOnInit() {
    this.AllocationsData = new MatTableDataSource();
    this.getAllocations();
  }

  getAllocations()  {
    this.ApiService.getAllocations().subscribe(data => this.AllocationsData.data = data)
  }
}
