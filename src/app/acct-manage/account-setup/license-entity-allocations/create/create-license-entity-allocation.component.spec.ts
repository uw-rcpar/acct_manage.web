import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLicenseGroupAllocationComponent } from './create-license-group-allocation.component';

describe('CreateLicenseGroupAllocationComponent', () => {
  let component: CreateLicenseGroupAllocationComponent;
  let fixture: ComponentFixture<CreateLicenseGroupAllocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLicenseGroupAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLicenseGroupAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
