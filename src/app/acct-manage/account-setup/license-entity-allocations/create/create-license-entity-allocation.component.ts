import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-license-entity-allocation',
  templateUrl: './create-license-entity-allocation.component.html',
  styleUrls: ['./create-license-entity-allocation.component.sass']
})
export class CreateLicenseEntityAllocationComponent implements OnInit {

  PageTitle: string = 'Create Entity Allocation';
  // form controls for form fields
  licenseGroupId = new FormControl('');
  accountEntityId = new FormControl('');
  quantity = new FormControl('');
  data: any;

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
  }

  createLicenseEntityAllocation() {
    let body = {
      "licenseGroupId": +this.licenseGroupId.value,
      "accountEntityId": +this.accountEntityId.value,
      "quantity": +this.quantity.value,
    };
    console.log(body);
    try {
      this.ApiService.createLicenseEntityAllocation(JSON.stringify(body))
        .subscribe(resp => {
            this.router.navigate(['/account-setup/license-entity-allocations/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}

