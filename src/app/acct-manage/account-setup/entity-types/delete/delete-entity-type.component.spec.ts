import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteEntityTypeComponent } from './delete-entity-type.component';

describe('DeleteEntityTypeComponent', () => {
  let component: DeleteEntityTypeComponent;
  let fixture: ComponentFixture<DeleteEntityTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteEntityTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteEntityTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
