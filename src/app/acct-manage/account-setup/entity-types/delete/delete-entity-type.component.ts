import {Component, OnDestroy, OnInit} from '@angular/core';
import { ApiService } from '../../../../api.service';

import { ActivatedRoute, Router } from '@angular/router';
import {IEntityType} from "../../IEntityType";

@Component({
  selector: 'app-delete-entity-type',
  templateUrl: './delete-entity-type.component.html',
  styleUrls: ['./delete-entity-type.component.sass']
})
export class DeleteEntityTypeComponent implements OnInit, OnDestroy {

  PageTitle: string = 'Delete Entity Type';
  id: number;
  private sub: any;
  data: any;
  entityType: IEntityType[] = [];

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getEntityType();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  getEntityType()  {
    try {
      this.ApiService.getEntityType(this.id)
        .subscribe(resp => {
            console.log(resp, "resp");
            this.entityType = resp
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  deleteEntityType() {
    try {
      this.ApiService.deleteEntityType(this.id)
        .subscribe(entity => {
            this.router.navigate(['/account-setup/entity-types/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
