import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEntityTypeComponent } from './create-entity-type.component';

describe('CreateEntityTypeComponent', () => {
  let component: CreateEntityTypeComponent;
  let fixture: ComponentFixture<CreateEntityTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEntityTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEntityTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
