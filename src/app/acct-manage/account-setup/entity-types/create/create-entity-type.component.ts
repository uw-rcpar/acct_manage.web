import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-create-entity-type',
  templateUrl: './create-entity-type.component.html',
  styleUrls: ['./create-entity-type.component.sass']
})
export class CreateEntityTypeComponent implements OnInit {

  PageTitle: string = 'Create Entity Type';
  // form controls for form fields
  name = new FormControl('', [Validators.required]);
  parentEntityTypeId = new FormControl('');
  data: any;

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
  }

  // function that returns error message
  getErrorMessage(): string {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  addEntityType() {
    let parentId = 0;
    if (this.parentEntityTypeId.value.length) {
      let parentId = this.parentEntityTypeId.value
    }
    let body = {
      "name": this.name.value,
      "parentEntityTypeId": parentId
    };
    try {
      this.ApiService.createEntityType(JSON.stringify(body))
        .subscribe(resp => {
            this.router.navigate(['/account-setup/entity-types/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
