import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { MatTableDataSource } from "@angular/material/table";
import {IEntityType} from "../../IEntityType";

@Component({
  selector: 'app-view-entity-types',
  templateUrl: './view-entity-types.component.html',
  styleUrls: ['./view-entity-types.component.sass']
})
export class ViewEntityTypesComponent implements OnInit {

  PageTitle: string = 'View Entity Types';
  EntityTypesData: any;
  data: IEntityType[] = [];
  displayedColumns: string[] = ['EntityTypeId', 'Name', 'ParentEntityTypeId', 'Actions'];

  constructor(private ApiService: ApiService) {
  }

  ngOnInit() {
    this.EntityTypesData = new MatTableDataSource();
    this.getEntityTypes();
  }

  public getEntityTypes()  {
    this.ApiService.getEntityTypes().subscribe(data => this.EntityTypesData.data = data)
  }

}
