import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEntityTypesComponent } from './view-entity-types.component';

describe('ViewEntityTypesComponent', () => {
  let component: ViewEntityTypesComponent;
  let fixture: ComponentFixture<ViewEntityTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewEntityTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEntityTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
