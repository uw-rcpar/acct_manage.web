import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEntityTypeComponent } from './edit-entity-type.component';

describe('EditEntityTypeComponent', () => {
  let component: EditEntityTypeComponent;
  let fixture: ComponentFixture<EditEntityTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditEntityTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEntityTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
