import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../../../api.service';
import { FormControl, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import {IEntityType} from "../../IEntityType";

@Component({
  selector: 'app-edit-entity-type',
  templateUrl: './edit-entity-type.component.html',
  styleUrls: ['./edit-entity-type.component.sass']
})
export class EditEntityTypeComponent implements OnInit, OnDestroy {

  PageTitle: string = 'Edit Entity Type';
  id: number;
  private sub: any;
  entityType: IEntityType[] = [];
  // form controls for form fields
  name = new FormControl(this.entityType['name'], [Validators.required]);
  parentEntityTypeId = new FormControl(this.entityType['parentEntityTypeId']);
  data: any;

  constructor(private ApiService: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']; // (+) converts string 'id' to a number
    });

    this.getEntityType();

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // function that returns error message
  getErrorMessage(): string {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  editEntityType() {
    // For some reason default form values are not being returned when submitting the form.
    // Get them from entityType if form value is empty.
    let name = this.entityType['name'];
    if (this.name.value) {
      name = this.name.value;
    }
    let parentEntityTypeId = +this.entityType['parentEntityTypeId'];
    if (this.parentEntityTypeId.value) {
      parentEntityTypeId = +this.parentEntityTypeId.value;
    }
    let body = {
      "entityTypeId": this.id,
      "name": name,
      "parentEntityTypeId": parentEntityTypeId,
      "parentEntityType": null
    };
    try {
      this.ApiService.editEntityType(body)
        .subscribe(resp => {
            this.router.navigate(['/account-setup/entity-types/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

  getEntityType()  {
    try {
      this.ApiService.getEntityType(this.id)
        .subscribe(entity => {
            this.entityType = entity
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
