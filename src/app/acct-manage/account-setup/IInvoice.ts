export interface IInvoice {
  invoiceId: number,
  accountId: number,
  saleAmount: number,
  contractDate: number,
  salesLeadId: number
}
