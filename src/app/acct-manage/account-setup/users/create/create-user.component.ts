import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.sass']
})
export class CreateUserComponent implements OnInit {

  PageTitle: string = 'Create User';
  // form controls for form fields
  name = new FormControl('', [Validators.required]);
  data: any;

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
  }

  // function that returns error message
  getErrorMessage():string {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  addUser() {
    let body = {
      "name": this.name.value,
      "verticalId": 0
    };
    try {
      this.ApiService.createAccount(JSON.stringify(body))
        .subscribe(resp => {
            this.router.navigate(['/account-setup/accounts/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
