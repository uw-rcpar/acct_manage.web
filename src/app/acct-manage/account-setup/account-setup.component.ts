import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-setup',
  templateUrl: './account-setup.component.html',
  styleUrls: ['./account-setup.component.sass']
})
export class AccountSetupComponent implements OnInit {

  PageTitle: string = 'Account Setup';

  constructor() { }

  ngOnInit() {
  }

}
