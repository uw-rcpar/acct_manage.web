import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ApiService } from '../../../../api.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-user-role',
  templateUrl: './create-user-role.component.html',
  styleUrls: ['./create-user-role.component.sass']
})
export class CreateUserRoleComponent implements OnInit {

  PageTitle: string = 'Create User Role';
  // form controls for form fields
  name = new FormControl('', [Validators.required]);
  context = new FormControl('');

  constructor( private ApiService: ApiService, private router: Router ) { }

  ngOnInit() {
  }

  // function that returns error message
  getErrorMessage():string {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  addUserRole() {
    let body = [{
      "userId": this.userId.value,
      "roleId": +this.roleId.value
    }];
    console.log(body);
    try {
      this.ApiService.createUserRole(JSON.stringify(body))
        .subscribe(resp => {
            console.log(resp);
            this.router.navigate(['/account-setup/roles/view']);
          },
          error => {
            console.log(error, "error");
          })
    } catch (e) {
      console.log(e);
    }
  }

}
