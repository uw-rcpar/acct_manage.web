export interface IRole {
  roleId: number,
  name: string,
  context: number,
  createdDate: number,
  updatedDate: number
}
