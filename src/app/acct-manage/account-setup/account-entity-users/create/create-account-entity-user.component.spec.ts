import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAccountEntityUserComponent } from './create-account-entity-user.component';

describe('CreateAccountEntityUserComponent', () => {
  let component: CreateAccountEntityUserComponent;
  let fixture: ComponentFixture<CreateAccountEntityUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAccountEntityUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccountEntityUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
